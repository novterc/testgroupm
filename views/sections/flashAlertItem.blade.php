<div class="alert alert-{{$type}} fade in alert-dismissable">
    <a class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <strong>
	@if( $type === 'info' )
		Info!
	@elseif( $type === 'danger')
		Danger!
	@elseif( $type === 'warning')
		Warning!
	@elseif( $type === 'success')
		Success!
	@endif
	</strong> {{$msg}}
</div>