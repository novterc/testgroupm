<!DOCTYPE html>
<html>
	<head>
		<title>@yield('title', '-no title-')</title>
		@yield('header')
	</head>
	<body>@yield('body')@yield('bottomScript')</body>
</html>