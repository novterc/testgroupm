@extends('templates.container')
@section('header')
	@parent
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="/main/css/news.css">
@endsection
@section('title')@parent Test work - groupM @endsection
@section('container')

	<div class="starter-template">
		<h1>Test work - groupM</h1>
		
		@if(!empty($rbcParserExtendData))
			<table class="table">
				<thead>
					<tr>
						<th>Titlte</th>
						<th>Time</th>
					</tr>
				</thead>
				<tbody>
					@foreach( $rbcParserExtendData as $item )
					<tr class="news-table-row news-tooltip-row" data-image="{{$item['image'] or ''}}" data-first-sentences="{{$item['firstSentences'] or ''}}">
						<td><a href="{{$item['href']}}" target="_blank">{{$item['title']}}</a></td>
						<td><a href="{{$item['href']}}" target="_blank">{{$item['time']}}</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		@endif
	</div>

@endsection
@section('bottomScript')
	@parent
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script src="/main/js/newsTooltip.js"></script>
@endsection
