@extends('templates.container')
@section('header')
	@parent
	<link rel="stylesheet" href="/main/css/numbers.css">
@endsection
@section('title')@parent Test work - groupM @endsection
@section('container')

	<div class="starter-template">
		<h1>Test work - groupM</h1>
		<div class="numbers-wrapper"></div>
	</div>

@endsection
@section('bottomScript')
	@parent
	<script src="/main/js/numbers.js"></script>
@endsection
