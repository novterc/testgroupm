$(document).tooltip({
	items:".news-tooltip-row",
	tooltipClass: "news-tooltip",
	content: function () {
		var text = $(this).data("first-sentences");
		var image = $(this).data("image");

		var content = $('<div>',{ class: 'news-tooltip-content' });
		var img = $('<img>',{ src: image});
		var p = $('<p>', { text: text});
		
		content.append(img);
		content.append(p);
		return content.get();
	}
});