(function(){

	var numMover = false;

	$('.numbers-wrapper').each(function(){
		numMover = new numbersMover(this);
	});

	function numbersMover($boxWrapper)
	{
		this.boxWrapper = $boxWrapper;
		var numbersMover = this;

		function init()
		{
			var indexsArr = createArrayIndexs(4);
			indexsArr = shuffle(indexsArr);
			var domIndexList = createItemsList(indexsArr);
			appendItemsList(domIndexList);
		}

		function appendItemsList($list)
		{
			for(var key in $list){
				var item = $list[key];
				$(numbersMover.boxWrapper).append(item);
				$(item).data('itemObj').setPosition  ();
			}
		}

		function createItemsList($list)
		{
			var domItemList = new Array();
			for(var key in $list){
				var index = $list[key];
				var domItem = createItem(index, (index+1));
				domItemList.push(domItem);
			}
			return domItemList;
		}

		function createItem($index, $content)
		{
			var el = $('<div>',{
				class: 'numbers-item',
				text: $content,
			});
			$(el).data('itemObj', new numbersMoverItem(el[0], $index));
			$(el).on('click', numbersMover.itemOnClick)
			return el;
		}

		function numbersMoverItem($domEl, $index)
		{
			var numbersMoverItem = this;
			this.domEl = $domEl;
			this.index = $index;
			this.position = 0;
			// this.status = 0;
			// this.moving = false;
			this.isOwnPosition = function()
			{
				if(this.index == $(this.domEl).index())
					return true;
				else
					return false;
			}

			this.setPosition = function()
			{
				this.position = $(this.domEl).index();
			}

			return this;
		}
		
		// function createItemPhantom($direct, $index)
		// {
		// 	var el = $('<div>',{
		// 		class: 'numbers-item-phantom',
		// 	});	
		// 	$(findItemByIndex($index)).before(el)
		// 	$(el).data('itemObj', new numbersMoverItemPhantom(el[0], $direct));
		// 	return el;
		// }

		function findItemByIndex($index)
		{
			return $(numbersMover.boxWrapper).find('div').eq($index).get(0);
		}

		function numbersMoverItemPhantom($domEl, $direct)
		{
			var numbersMoverItemPhantom = this;
			this.domEl = $domEl;
			this.direct = $direct;

			return this;
		}

		function createArrayIndexs($size)
		{
			var indexArr = new Array();
			for (var i=0; i<=$size; i++) {
				indexArr.push(i);
			}
			return indexArr;
		}

		function shuffle($array)
		{
			var currentIndex = $array.length, temporaryValue, randomIndex;
			while (0 !== currentIndex) {
				randomIndex = Math.floor(Math.random() * currentIndex);
				currentIndex -= 1;

				temporaryValue = $array[currentIndex];
				$array[currentIndex] = $array[randomIndex];
				$array[randomIndex] = temporaryValue;
			}
			return $array;
		}

		this.itemOnClick = function()
		{
			var itemObj = $(this).data('itemObj');
			if(itemObj.isOwnPosition()){
				numbersMover.itemMoveTop(this);
			} else {
				numbersMover.itemMoveOwn(this);
			}
		}

		this.itemMoveTop = function($el)
		{
			moveToIndex($el, 0);
		}

		this.itemMoveOwn = function($el)
		{
			var itemObj = $($el).data('itemObj');
			moveToIndex($el, itemObj.index);
		}

		function moveToIndex($el, $index)
		{
			$el = $($el).detach();
			if($index == 0)
				$(findItemByIndex(0)).before($el);
			else
				$(findItemByIndex(($index-1))).after($el);
		}

		init();

		return this;
	}
	

}())
