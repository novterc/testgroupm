<?php

namespace App\Core;

class View
{
	protected $blade;

	public function __construct($viewsPath, $cachePath)
	{
		$blade = new \Philo\Blade\Blade( $viewsPath, $cachePath );
		$this->blade = $blade;
	}

	public function render( $template, $values=[] )
	{
		echo $this->blade->view()->make( $template, $values )->render();
	}
	
}
