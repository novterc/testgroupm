<?php

namespace App\Core;

use app\App;

class AppChildren
{
	public function __construct()
	{
		$this->app = App::get();
		$this->view = $this->app->view;
		$this->config = $this->app->config;
		$this->cache = $this->app->cache;
	}

	public function abort($str)
	{
		$this->app->abort($str);
	}

	public function conf($path)
	{
		return $this->app->conf($path);
	}
}
