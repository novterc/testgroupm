<?php

namespace App\Controllers;

use app\Core\Controller;
use app\Helpers\RbcParserWrapper;

class HomeController extends Controller
{

	public function index()
	{
		$rbcParserWrapper = new RbcParserWrapper();
		$this->view->render('pages.main', [
			'rbcParserExtendData' => $rbcParserWrapper->extendDataMainPage(),
		]);
	}

	public function numbers()
	{
		$this->view->render('pages.numbers');
	}

}
