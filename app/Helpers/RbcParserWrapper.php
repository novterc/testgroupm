<?php

namespace App\Helpers;

use app\Helpers;
use app\Core\AppChildren;

class RbcParserWrapper extends AppChildren
{

	public function __construct()
	{
		parent::__construct();
		$this->mainPage = new Helpers\RbcParserMainPage();
		$this->newsPage = new Helpers\RbcParserNewsPage();
	}

	public function extendDataMainPage()
	{
		$cacheKey = 'helpers.rbcParserWrapper.data';
		$resData = $this->cache->get($cacheKey);
		if($resData===null){
			if(Helpers\MultiCallCommands::checkAble()){
				$this->extendDataMainPageMultiProcess();
				$resData = $this->extendDataMainPageSinglProcess();
			} else {
				$resData = $this->extendDataMainPageSinglProcess();
			}
		}
		
		if(!empty($resData)){
			$this->cache->put($cacheKey, $resData, $this->conf('cacheExp.rbcParserExtendDataMainPage'));
		}

		return $resData;
	}

	public function extendDataMainPageMultiProcess()
	{
		$data = $this->mainPage->getDataOrCache();
		if(empty($data))
			return false;
		foreach ($data as $key => $item) {
			$hrefList[] = $item['href'];
		}
		Helpers\MultiCallCommands::runCommands($hrefList);
	}

	public function extendDataMainPageSinglProcess()
	{
		$data = $this->mainPage->getDataOrCache();
		if(empty($data))
			return false;

		$resData = false;
		foreach ($data as $key => $item) {
			$dataNewsPage = $this->newsPage->getDataOrCache($item['href']);
			if(empty($dataNewsPage['firstSentences']))
				continue;
			$resData = $this->mainPage->setFirstSentencesByUrl(false, $item['href'], $dataNewsPage['firstSentences']);
		}

		return $resData;
	}

}
