<?php

namespace App\Helpers;

use app\App;
use app\Core\Helpers;
use app\Helpers\RbcParserNewsPage;

class MultiCallCommands extends Helpers
{

	public static function checkAble()
	{
		if(function_exists('exec'))
			return true;
		else
			return false;		
	}

	public static function runCommands($list)
	{
		$basePath = App::get()->basePath;
		$command = 'php "'.$basePath.'/app/Commands/loadRbcNewsPage.php"';
		$commandsList = [];
		
		foreach ($list as $href) {
			$commandData =' "'.$href.'" >> '.$basePath.'/storage/log 2>>'.$basePath.'/storage/log &';
			exec($command.$commandData);
		}

		return self::whait($list);
	}

	public static function whait($list)
	{
		$timeout = App::get()->conf('multiloadRbcLoadTimeout');
		$rbcParserNewsPage = new RbcParserNewsPage();
		while(true){
			$fin = true;
			foreach ($list as $href) {
				if(empty($rbcParserNewsPage->getCache($href))){
					$fin = false;
					break;
				} 
			}
			if($fin)
				return true;
			else
				sleep(1);

			$timeout--;
			if( $timeout === 0 )
				return false;
		}
	}

}
