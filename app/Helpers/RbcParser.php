<?php

namespace App\Helpers;

use app\App;
use app\Core\Helpers;

class RbcParser extends Helpers
{

	protected function getHtmlByUrl($url)
	{
		return file_get_contents($url);
	}

}
