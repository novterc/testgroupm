<?php

namespace App\Helpers;

use app\Helpers\RbcParser;
use Sunra\PhpSimple\HtmlDomParser;

class RbcParserMainPage extends RbcParser
{
	protected static $cacheKey = 'helpers.RbcParserMainPage.data';

	public function getData() 
	{
		$html = $this->getHtml();
		if(empty($html))
			$this->abort('getHtml returned an empty result.');

		$data = $this->parser($html);
		return $data;
	}

	public function getDataOrCache() 
	{
		$data = $this->cache->get(self::$cacheKey);
		if($data===null){
			$data = $this->getData();
			$this->cache->put(self::$cacheKey, $data, $this->conf('cacheExp.rbcParserMainPage'));
		}
		return $data;
	}

	public function setFirstSentencesByUrl($data, $url, $sentences)
	{
		if($data===false)
			$data = $this->getDataOrCache();

		if(empty($data))
			return false;

		foreach ($data as $key => $item) {
			if($item['href'] === $url){
				$data[$key]['firstSentences'] = $sentences; 
			}
		}

		$this->cache->put(self::$cacheKey, $data, $this->conf('cacheExp.rbcParserMainPage'));
		return $data;
	}

	protected function getHtml()
	{
		$url = 'http://top.rbc.ru/';
		$html = $this->getHtmlByUrl($url);
		return $html;
	}

	protected function parser($html)
	{
		$resultItems = [];
		$dom = HtmlDomParser::str_get_html( $html );
		$items = $dom->find('.l-col-main .l-col-center .item_medium a');
		if(!empty($items)){
			foreach ($items as $item) {
				$itemParseResult = $this->parserItem($item);
				$resultItems[] = $itemParseResult;	
			}
		}
		return $resultItems;
	}

	protected function parserItem($itemDom)
	{
		$result = [];
		$result['href'] = $itemDom->attr['href'];
		$result['title'] = trim($itemDom->find('.item_medium__title', 0)->innertext);
		$result['time'] = trim($itemDom->find('.item_medium__time', 0)->innertext);
		$image = $itemDom->find('img.item_medium__image', 0);
		if(!empty($image))
			$result['image'] = $image->attr['src'];
		return $result;
	}

}
