<?php

namespace App\Helpers;

use app\Helpers\RbcParser;
use Sunra\PhpSimple\HtmlDomParser;

class RbcParserNewsPage extends RbcParser
{

	public function getData($url)
	{
		$html = $this->getHtmlByUrl($url);
		if(empty($html))
			$this->abort('getDataNewsPage returned an empty result.');
		$data = $this->parser($html);
		$data['firstSentences'] = $this->getFirstSentences($data['firstText'], $this->conf('rbcParser.countFirstSentences'));
		return $data;
	}

	public function getDataOrCache($url) 
	{
		$cacheKey = 'helpers.rbcParserNewsPage.'.md5($url).'.data';
		$data = $this->cache->get($cacheKey);
		if($data===null){
			$data = $this->getData($url);
			$this->cache->put($cacheKey, $data, $this->conf('cacheExp.rbcParserNewsPage'));
		}
		return $data;
	}

	public function getCache($url) 
	{
		$cacheKey = 'helpers.rbcParserNewsPage.'.md5($url).'.data';
		$data = $this->cache->get($cacheKey);
		return $data;
	}

	protected function parser($html)
	{
		$resultData = [];
		$dom = HtmlDomParser::str_get_html( $html );
		$content = $dom->find('.l-col-main .l-col-center .article__content', 0);
		if(!empty($content)){
			$overview = $content->find('.article__text__overview', 0);
			$firstText = '';
			if(!empty($overview))
				$firstText = $overview->plaintext;
		
				$firstText .= $content->plaintext; 
				$resultData['firstText'] = $firstText;
		}
		return $resultData;
	}

	public function getFirstSentences($text, $count=1)
	{
		$pattern = '/.+?[^\.](\. ?|$)/';
		$matches = false;
		preg_match_all($pattern, $text, $matches);
		if(!empty($matches[0])){
			$slice = array_slice($matches[0], 0, $count);
			return implode('', $slice);
		}
	}

}
