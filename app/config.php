<?php

return [

	'app' => [
		'paths' => [
			'viewTemplatesFolder' => $this->basePath.'/views',
			'viewCahceFolder' => $this->basePath.'/storage/bladeCache',
			'cacheFoler' => $this->basePath.'/storage/appCache',
		],
	],

	'rbcParser' => [
		'countFirstSentences' => 3,
	],

	'cacheExp' => [
		'rbcParserExtendDataMainPage' => 5,
		'rbcParserMainPage' => 5,
		'rbcParserNewsPage' => 60*24,
	],

	'mysql' => [
		'default' => [
			'localhost' => 'localhost',
			'basename' => 'testdb',
			'user' => 'root',
			'password' => 'passwordd',
		],
	],
	
	'multiloadRbcLoadTimeout' => 50,
	
];
