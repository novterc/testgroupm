<?php

namespace App;

use app\Core;
use app\Controllers;
use app\Data\Mysql;

class App
{
	private static $instance;

	private function __clone() {}
	private function __wakeup() {}
	private function __construct() {
		$this->basePath = dirname(__DIR__);
		$this->config = include 'config.php';
		$this->view = new Core\View($this->conf('app.paths.viewTemplatesFolder'), $this->conf('app.paths.viewCahceFolder'));
		$this->cache = $this->getIlluminateCahce();	
		$this->mysql = Mysql::instance($this->conf('mysql'), 'default');
	}

	protected function getIlluminateCahce()
	{
		$container = new \Illuminate\Container\Container();
		$container->singleton('files', function(){
			return new \Illuminate\Filesystem\Filesystem();
		});

		$container->singleton('config', function(){
			return [
				'cache.default' => 'files',
				'cache.stores.files' => [
					'driver' => 'file',
					'path' => $this->conf('app.paths.cacheFoler'),
				]
			];
		});

		$cacheManager = new \Illuminate\Cache\CacheManager($container);
		return $cacheManager->driver();
	}

	public static function get()
	{
		if(is_null(self::$instance)){
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function abort($str)
	{
		throw new \Exception($str);
	}

	public function conf($path) 
	{
		$keyArr = explode('.', $path);
		$config = $this->config;
		foreach ($keyArr as $key) {
			if(!isset($key))
				$this->abort('undefined config path:'.$path);
			$config = $config[$key];
		}

		return $config;
	}

	public function httpRun()
	{
		$controller = new Controllers\HomeController();
		if( !empty($_GET['method']) && $_GET['method'] === 'numbers')
			$controller->numbers();
		else
			$controller->index();
	}

}
