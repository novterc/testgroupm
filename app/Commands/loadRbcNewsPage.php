<?php

require dirname(__DIR__ ).'/bootstrap.php';

use app\Helpers\RbcParserNewsPage;

if(empty($argv[1]))
	$App->abort('undefined url');
$url = $argv[1];

$rbcParserNewsPage = new RbcParserNewsPage();
$data = $rbcParserNewsPage->getDataOrCache($url);
if(empty($data['firstSentences']))
	$App->abort('undefined firstSentences');

echo '.';
