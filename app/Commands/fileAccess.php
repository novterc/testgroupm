<?php

require dirname(__DIR__ ).'/bootstrap.php';

$filePath = $App->basePath.'/storage/testFileAccess.txt';

$fp = fopen($filePath, "r+");
if($fp){
	while(true) {
		if (flock($fp, LOCK_EX)) {

			$text = '';
			while (!feof($fp)){
				$text .= fgets($fp, 999);				
			}

			$rows = explode("\n", $text);
			$rows = array_map('intval', $rows);

			$sum = 0;
			array_map(function($item) use(&$sum){ $sum += $item; }, $rows);
			echo "\r\n{$sum}\r\n";

			fwrite($fp, "\n{$sum}");
			fflush($fp);
			flock($fp, LOCK_UN);
			break;
		}
		sleep(1);
	}
} else {
	$App->abort('Error get read file');
}
fclose($fp);
