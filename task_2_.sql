CREATE TABLE `info` (

`id` int(11) NOT NULL auto_increment,

`name` varchar(255) default NULL,

`desc` text default NULL,

PRIMARY KEY (`id`)

) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

CREATE TABLE `data` (

`id` int(11) NOT NULL auto_increment,

`date` date default NULL,

`value` INT(11) default NULL,

PRIMARY KEY (`id`)

) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

CREATE TABLE `link` (

`data_id` int(11) NOT NULL,

`info_id` int(11) NOT NULL

) ENGINE=MyISAM DEFAULT CHARSET=cp1251;




ALTER TABLE `data`
COLLATE='utf8_general_ci',
ENGINE=InnoDB;

ALTER TABLE `info`
COLLATE='utf8_general_ci',
ENGINE=InnoDB;

ALTER TABLE `link`
COLLATE='utf8_general_ci',
ENGINE=InnoDB;

ALTER TABLE `link`
	ADD UNIQUE INDEX `uniq_comb` (`data_id`, `info_id`);

ALTER TABLE `link`
	ADD CONSTRAINT `FK_link_data` FOREIGN KEY (`data_id`) REFERENCES `data` (`id`) ON DELETE CASCADE;

ALTER TABLE `link`
	ADD CONSTRAINT `FK_link_info` FOREIGN KEY (`info_id`) REFERENCES `info` (`id`) ON DELETE CASCADE;


select * from link tb1 
LEFT JOIN `data` tb2 ON tb2.id = tb1.data_id
LEFT JOIN `info` tb3 ON tb3.id = tb1.info_id;